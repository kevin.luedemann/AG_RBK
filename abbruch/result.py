#!/usr/bin/env python
# coding: utf8
import numpy as np
import matplotlib.pyplot as plt

def main():
    height = [0,5,10,15,20,25,30,35,40,45,50,60,70,80,90,100,110,120,220,320,420,520,620,720,820,920,1020]
    height = np.array(height,dtype="float")/10/1000

    data = [[0.735,0.74,0.758],
            [0.995,0.978,0.896],
            [1.23,1.3,1.32],
            [1.52,1.43,1.42],
            [1.33,1.33,1.27],
            [1.48,1.47,1.35],
            [1.75,1.74,1.63],
            [1.96,2.15,1.98],
            [1.95,2.19,2.05],
            [1.88,1.9,1.88],
            [1.56,1.6,1.6],
            [1.56,1.61,1.58],
            [1.96,1.83,1.96],
            [1.78,2.11,1.84],
            [1.87,1.9,1.78],
            [1.84,1.86,1.74],
            [1.61,1.56,1.56],
            [2,2.03,1.92],
            [1.2,1.16,1.16],
            [1.34,1.29,1.3],
            [0.76,0.8,0.8],
            [0.3,0.38,0.34],
            [0.33,0.35,0.29],
            [0.26,0.31,0.26],
            [0.35,0.37,0.34],
            [0.32,0.29,0.29],
            [0.2,0.26,0.21]]
            #[1.21,1.22,1.28],
            #[1.5,1.68,1.4]]

    f,ax    = plt.subplots()
    ax.errorbar(height,np.mean(data,axis=1),yerr=np.std(data,axis=1),fmt="r-",capsize=5)
    ax.set_xlabel(u"Höhe/m")
    ax.set_ylabel("Abbruchfrequenz/Hz")
    ax.grid("on")
    f.savefig("abbruch_result.png",dpi=300,bbox_inches="tight")
    plt.show()

if __name__ == "__main__":
    main()
