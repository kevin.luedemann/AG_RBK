import numpy as np
import matplotlib.pyplot as plt

def main():
    data    = np.genfromtxt("../18-06-06/Power_0050.dat")
    data2   = np.genfromtxt("../18-06-06/Powerfq_0050.dat")
    mask_ab = np.logical_and(data[:,0]>7*10**(-1),data[:,0]<1.0*10**(0))
    mask_no = data[:,0]>2*10**(0)

    nup_ab  = np.polyfit(np.log(data[:,0][mask_ab]),np.log(data[:,1][mask_ab]),1)
    nup_no  = np.polyfit(np.log(data[:,0][mask_no]),np.log(data[:,1][mask_no]),1)
    print nup_ab
    print nup_no


    xs_ab = [3*10**(-1),2.5*10**0]
    xs_no = [1.,5.]
    f,ax = plt.subplots()
    ax.loglog(data[:,0],data[:,1],'r',label="Powerspektrum bei z=5cm")
    ax.loglog(data2[:,0],data2[:,1],'y',label="Powerspektrum mit Frequenzquadrat bei z=5cm")
    ax.set_xlabel("Frequenz/Hz")
    ax.set_ylabel("Powerspektrum")
    ax.plot(xs_ab,np.exp(np.polyval(nup_ab,np.log(xs_ab))),"b",
            label="Wendetangente")
    #ax.plot(xs_no,np.exp(np.polyval(nup_no,np.log(xs_no))),"k",
    ax.plot(xs_no,np.ones_like(xs_no)*np.exp(nup_no[1]),"k",
            label="Rauschpegel")
    ax.grid('on',which="both")
    ax.legend(loc="best")
    f.savefig("abbruchfrequenz.png",dpi=300,bbox_inches="tight")

    plt.show()

if __name__ == "__main__":
    main()
