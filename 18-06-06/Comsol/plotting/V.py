import numpy as np
import matplotlib.pyplot as plt
from glob import glob

def main():
    files = sorted(glob("../V*"))
    V = []
    f,ax = plt.subplots()
    numbers = ["$Ra=10^3$","$Ra=10^4$","$Ra=10^5$","$Ra=10^6$","$Ra=10^7$"]
    #for fi in files:
    for i in range(len(files)):
        fi = files[i]
        #name = fi.split("/")[-1][:-4]
        name = numbers[i]
        V = np.genfromtxt(fi,comments="%")
        itdx,col = V.shape
        V = V.reshape(itdx/200,200,col)
        V = np.mean(V,axis=0)
        ax.plot(V[:,0],V[:,1],label=name)
    ax.legend(loc='best')
    ax.set_xlabel("Hoehe")
    ax.set_ylabel("Geschwindigkeit")
    #f.savefig("V.png",dpi=300,bbox_inches="tight")
    plt.show()

if __name__ == "__main__":
    main()
