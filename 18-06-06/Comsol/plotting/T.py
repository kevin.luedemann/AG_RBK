import numpy as np
import matplotlib.pyplot as plt
from glob import glob

def main():
    Nu_T = np.array([.5,.35,.2,.1,.05])
    files = sorted(glob("../T*"))
    T = []
    f,ax = plt.subplots()
    #for fi in files:
    numbers = ["$Ra=10^3$","$Ra=10^4$","$Ra=10^5$","$Ra=10^6$","$Ra=10^7$"]
    fmts = ['C0','C1','C2','C3','C4']
    for i in range(len(files)):
        fi = files[i]
        #name = fi.split("/")[-1][:-4]
        name = numbers[i]
        T = np.genfromtxt(fi,comments="%")
        itdx,col = T.shape
        T = T.reshape(itdx/200,200,col)
        T = np.mean(T,axis=0)
        ax.plot(T[:,0],T[:,1],fmts[i]+"-",label=name)
        ax.axvline(Nu_T[i],0,0.55,color=fmts[i])
        ax.axvline(1.-Nu_T[i],0,0.55,color=fmts[i])
    ax.legend(loc='best')
    ax.set_xlabel("Hoehe")
    ax.set_ylabel("Temperatur")
    f.savefig("T.png",dpi=300,bbox_inches="tight")
    plt.show()

if __name__ == "__main__":
    main()
