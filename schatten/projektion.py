#!/usr/bin/env python
# coding: utf8
import numpy as np
from tabulate import tabulate

def correction(B=0.05,QZ=2.,QW=2.90):
    return QZ * B/(QW)

def geschw(ti,QZ,QW):
    return correction(QZ=QZ,QW=QW)/ti

def reynolds(v,L=0.2,nu=1e-6):
    return np.sqrt(v*L/nu)

def main():
    #distance 0.05
    ost_west_dist   = [2.55-0.05, 2.93+0.05]  # distance projector - [cell, Leinwand]
    ost_west_auf    = [5.269, 5.08, 4.3, 5.45, 5.4, 4.27, 4.87, 6.45, 5.29, 3.4]
    ost_west_ab     = [7.9, 5.0, 5.25, 6.65, 7.09, 5.2, 4.83, 4.2, 4.39, 7.35,
                       5.47, 8.5, 5.7, 7.92]
    sud_nord_dist   = [2.75+0.05, 3.19-0.05]
    sud_nord_auf    = [4.9, 5.8, 4.0]
    sud_nord_ab     = [5.16, 6.5, 5.65, 6.12, 6.43, 6.16, 6.07, 5.6]

    ost_west_auf_gesch = []
    for tim in ost_west_auf:
        ost_west_auf_gesch.append(geschw(tim,ost_west_dist[0],ost_west_dist[1]))
    ost_west_ab_gesch = []
    for tim in ost_west_ab:
        ost_west_ab_gesch.append(geschw(tim,ost_west_dist[0],ost_west_dist[1]))
    sud_nord_auf_gesch = []
    for tim in sud_nord_auf:
        sud_nord_auf_gesch.append(geschw(tim,sud_nord_dist[0],sud_nord_dist[1]))
    sud_nord_ab_gesch = []
    for tim in sud_nord_ab:
        sud_nord_ab_gesch.append(geschw(tim,sud_nord_dist[0],sud_nord_dist[1]))
    OWauf_m = np.mean(ost_west_auf_gesch)
    OWauf_s = np.std(ost_west_auf_gesch)
    OWab_m = np.mean(ost_west_ab_gesch)
    OWab_s = np.std(ost_west_ab_gesch)
    SNauf_m = np.mean(sud_nord_auf_gesch)
    SNauf_s = np.std(sud_nord_auf_gesch)
    SNab_m = np.mean(sud_nord_ab_gesch)
    SNab_s = np.std(sud_nord_ab_gesch)
    print "OWauf"
    print OWauf_m,OWauf_s,reynolds(OWauf_m)
    print "OWab"
    print OWab_m,OWab_s,reynolds(OWab_m)
    print "SNauf"
    print SNauf_m,SNauf_s,reynolds(SNauf_m)
    print "SNab"
    print SNab_m,SNab_s,reynolds(SNab_m)

    headers = ["Richtung","Geschwindigkeit/$ms^{-1}$","Reynolds"]

    OWauf = [   "Ost-West auf$",
                "{:.05f}({:.05f})".format(OWauf_m,OWauf_s),
                "{}".format(reynolds(OWauf_m))
            ]
    OWab  = [   "Ost-West ab",
                "{:.05f}({:.05f})".format(OWab_m,OWab_s),
                "{}".format(reynolds(OWab_m))
            ]
    SNauf = [   u"Süd-Nord auf",
                "{:.05f}({:.05f})".format(SNauf_m,SNauf_s),
                "{}".format(reynolds(SNauf_m))
            ]
    SNab  = [   u"Süd-Nord ab",
                "{:.05f}({:.05f})".format(SNab_m,SNab_s),
                "{}".format(reynolds(SNab_m))
            ]

    table   = np.array([headers,OWauf,OWab,SNauf,SNab])
    print tabulate(table,headers="firstrow",tablefmt="latex_raw")


if __name__ == "__main__":
    main()
