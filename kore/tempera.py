#!/usr/bin/env python
# coding: utf8
import numpy as np
import matplotlib.pyplot as plt

def main():
    data = np.genfromtxt("../18-06-06/tem_notfallkorrelation.dat")
    value = []
    hoehe = np.array([0.02,0.05,0.08,0.12,0.15,0.18])
    for i in range(1,6):
        value.append(np.array([(hoehe[i-1]+hoehe[i])/2.,np.mean(data[:,i]),np.std(data[:,i])]))
    value = np.array(value)

    f,ax = plt.subplots()
    ax.fill_between(value[:,0],value[:,1]-value[:,2],value[:,1]+value[:,2])
    ax.plot(value[:,0],value[:,1],'r+-')
    ax.set_xlabel(u"Höhe/m")
    ax.set_ylabel("Temperatur/C")
    ax.grid('on',which="both")
    f.savefig("T_kor.png",dpi=300,bbox_inches="tight")
    plt.show()

if __name__ == "__main__":
    main()
