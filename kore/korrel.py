#!/usr/bin/env python
# coding: utf8
import numpy as np
import matplotlib.pyplot as plt
from glob import glob
from scipy.optimize import curve_fit

def gauss(x,a,x0,sigma):
    return a*np.exp(-(x-x0)**2/(2.*sigma**2))

def main():
    data = np.genfromtxt("../18-06-06/Geschwindigkeit_notfallkorrelation.dat")
    value = []
    sigma = []
    hoehe = np.array([0.02,0.05,0.08,0.12,0.15,0.18])
    param = []
    for i in range(1,6):
        #value.append(np.array([(hoehe[i-1]+hoehe[i])/2.,(hoehe[i]-hoehe[i-1])/data[np.argmax(data[:,i]),0]]))
        mean    = data[np.argmax(data[:,i]),0]
        mask    = (data[:,0]>mean-3.) & (data[:,0]<mean+3.)
        #fit gaussian
        popt,pcov = curve_fit(gauss,data[mask,0],data[mask,i],p0=[1.,mean,1.])

        value.append(np.array([(hoehe[i-1]+hoehe[i])/2.,(hoehe[i]-hoehe[i-1])/popt[1]]))
        sigma.append((hoehe[i]-hoehe[i-1])/popt[1]**2*popt[2])
        param.append(popt)
    value = np.array(value)
    sigma = np.array(sigma)
    param = np.array(param)
    print param


    f,ax = plt.subplots()
    #ax.plot(value[:,0],value[:,1],'r+-')
    #ax.errorbar(value[:,0],value[:,1],yerr=sigma/4.,fmt='r',capsize=5)
    ax.plot(value[:,0],value[:,1],'r+-')
    ax.set_xlabel(u"Höhe/m")
    ax.set_ylabel(r"Geschwindigkeit/ms$^{-1}$")
    ax.grid('on')
    f.savefig("v_kor.png",dpi=300,bbox_inches="tight")

    f,ax = plt.subplots()
    for i in range(1,6):
        x = np.arange(param[i-1,1]-5.,param[i-1,1]+5.,0.1)
        ax.plot(x,gauss(x,*param[i-1]),'k')
        ax.plot(data[:,0],data[:,i],label="T{}*T{}($\mu=${:.01f},$\sigma=${:.01f}s)".format(i,i+1,param[i-1,1],param[i-1,2]))
    ax.set_xlabel(r"$\tau/$s")
    ax.set_ylabel(r"$\int(T(t)T(t-\tau)$d$t$")
    ax.legend(loc="best")
    ax.grid('on')
    f.savefig("kor.png",dpi=300,bbox_inches="tight")
    plt.show()

if __name__ == "__main__":
    main()
