# What needs to be done for RBK

## during experiment

* write down plattentemperature output multiple times
* measure 10 times up and down velocity in the shadow projection
* take time series in center cell with 250Hz and assume abtastfrequenz
* measure temperature profile in cell, change spacing as going along, for f_c use a few time twice the amount of points

## Rayleigh number experiment

* read in the temperature file
* calculate temperature file

## shadow projection

* describe what has been seen
* calculate velocities and reynoldsnumber

## Abtastfrequenz

* what störfrequenzen at 250Hz
* at what frequencies do they appear in the spektrum at the choosen abtastrate
* how good was that choice

## temperature profile

* draw the rms and avg of the experiment
* draw numerical data and flow field
* compare rms to korrelation data for cell center

## Velocitie profile

* draw the profile from f_c
* is there a randschicht
* what is necessary for f_c to deliver correct velocity field

## velocity from korrelation

* calculate vel from korrelation data
* compare to shadow method
* height dependence

## compare velocities

* free fall velocity
* viscose fall

## Nusselt

* from depth of thermal boudary calculate Nusselt and estimate error
* how Nusselt numerically
* compare values
* plot Nu(Ra) and fit scaling law

## Temperature histogram

* plot temperature histogram from korrelation data
* compare different heights
