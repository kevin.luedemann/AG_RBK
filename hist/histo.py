#!/usr/bin/env python
# coding: utf8
import numpy as np
import matplotlib.pyplot as plt

def main():
    data = np.genfromtxt("../18-06-06/tem_notfallkorrelation.dat")
    f,ax = plt.subplots()
    hoehe = np.array([0.02,0.05,0.08,0.12,0.15,0.18])
    for i in range(1,7):
        ax.hist(data[:,i],log=True,histtype="step",label="{}m".format(0.2-hoehe[i-1]),bins=30)#,normed=True)
    ax.legend(loc="best")
    ax.set_xlabel("Temperatur/C")
    ax.set_ylabel(u"Häufigkeit")
    #ax.grid("on",which="both")
    f.savefig("hist.png",dpi=300,bbox_inches="tight")
    plt.show()
    

if __name__ == "__main__":
    main()
