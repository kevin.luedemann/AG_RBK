import numpy as np

def theta(U,R1=1e4,R2=2e4,U0=1):
    k   = R2/R1
    uk  = U/U0+1./(1.+k)
    return (uk+k*uk-1.)/(1.-uk)

def main():
    print theta(0.103)

if __name__ == "__main__":
    main()
