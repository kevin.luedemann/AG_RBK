import numpy as np
from tabulate import tabulate

def kappa(lamb,cp,rho):
    return lamb/cp/rho

def Pr(nu,kappa):
    return nu/kappa

def Ra(alpha,dT,L,kappa,nu,g=9.81):
    return g*alpha*dT*L**3/kappa/nu

def make_table():
    names   = np.array([""       ,"Zelle"  ,"Erdkern"  ,"Erdmantel"    ,"Atmosph\\\"are"])
    alpha   = np.array([          2.1e-4   ,1.2e-5     ,1.5e-5         ,3.7e-3])
    rho     = np.array([          1000     ,12000      ,5000           ,1.29])
    cp      = np.array([          4187     ,800        ,1200           ,1000])
    lamb    = np.array([          0.6      ,30         ,10             ,0.03])
    mu      = np.array([          1e-3     ,12e-3      ,1e21           ,1.7e-5])
    hoehe   = np.array([          0.2      ,2260e3     ,2855e3         ,15e3])
    dT      = np.array([          10.      ,2000       ,1000           ,65])

    kap     = kappa(lamb,cp,rho)
    nu      = mu/rho 
    Pr_     = Pr(nu,kap)
    Ra_     = Ra(alpha,dT,hoehe,kap,nu)

    alpha   = np.append(["$\\alpha/\\si{\\per\\kelvin}$"],alpha)
    rho     = np.append(["$\\rho/\\si{\\kilo\\gram\\per\\cubic\\meter}$"],rho)
    cp      = np.append(["$c_p/\\si{\\joule\\per\\kilo\\gram\\per\\kelvin}$"],cp)
    lamb    = np.append(["$\\lambda\\si{\\watt\\per\\meter\\per\\kelvin}$"],lamb)
    mu      = np.append(["$\\mu/\\si{\\pascal\\second}$"],mu)
    nu      = np.append(["$\\nu/\\si{\\square\\meter\\per\\second}$"],nu)
    kap     = np.append(["$\\kappa/\\si{\\square\\meter\\per\\second}$"],kap)
    hoehe   = np.append(["$L/\si{\\meter}$"],hoehe)
    dT      = np.append(["$dT/\\si{\\kelvin}$"],dT)
    Pr_     = np.append(["$Pr$"],Pr_)
    Ra_     = np.append(["$Ra$"],Ra_)

    table   = np.array([names,alpha,rho,cp,lamb,mu,nu,kap,hoehe,dT,Pr_,Ra_])


    print tabulate(table,headers="firstrow",tablefmt="latex_raw")

def main():
    #make_table()


if __name__ == "__main__":
    main()
