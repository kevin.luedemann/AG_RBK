import numpy as np
import matplotlib.pyplot as plt

def messfreq(f,fm=9.1):
    return np.mod(f,fm)

def main():
    data4 = np.genfromtxt("../18-06-06/Power_SF4.dat")
    f,ax = plt.subplots()
    ax.loglog(data4[:,0],data4[:,1],'r')
    ax.set_xlabel("Frequenz/Hz")
    ax.set_ylabel("Powerspektrum")
    ax.grid('on',which="both")
    #f.savefig("stoer.png",dpi=300,bbox_inches="tight")

    f,ax = plt.subplots()
    ax.loglog(messfreq(data4[:,0][data4[:,0]>=9.1]),data4[:,1][data4[:,0]>=9.1],'r.',label="Frequenzen ohne Aliasing")
    ax.loglog(data4[:,0][data4[:,0]<9.1],data4[:,1][data4[:,0]<9.1],'k-',label="Frequenzen die Aliasing unterliegen")
    ax.set_xlabel("Frequenz/Hz")
    ax.set_ylabel("Powerspektrum")
    ax.legend(loc='best')
    ax.grid('on',which="both")
    #f.savefig("stoer_shift.png",dpi=300,bbox_inches="tight")
    plt.show()

if __name__ == "__main__":
    main()
